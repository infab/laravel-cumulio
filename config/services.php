<?php

return [
    'cumulio' => [
        'api_key' => env('CUMULIO_KEY', 'Your key'),
        'api_secret' => env('CUMULIO_SECRET', 'Your secret/token')
    ]
];