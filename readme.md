### Cumulio SDK wrapper for Laravel

[![Latest Stable Version](https://poser.pugx.org/infab/laravel-cumulio/v/stable)](https://packagist.org/packages/infab/laravel-cumulio)
[![Total Downloads](https://poser.pugx.org/infab/laravel-cumulio/downloads)](https://packagist.org/packages/infab/laravel-cumulio)

Add the service provider to your app config (`config/app.php`)
```php
    # ...
    Infab\LaravelCumulio\CumulioServiceProvider::class,
    # ...
```

Add your credentials to the services config (`config/services.php`)

```php
    'cumulio' => [
        'api_key' => env('CUMULIO_KEY', 'Your key'),
        'api_secret' => env('CUMULIO_SECRET', 'Your secret/token')
    ]
```


**Usage**
```php
    $client = Cumulio::initialize();

    $time = new \DateTime();
    $securables[] = $dashboard_id;
    $authorization = $client->create('authorization', [
        'type' => 'temporary',
        'expiry' => $time->modify('+5 minutes')->format('c'),
        'securables' => $securables,
        'locale_id' => 'en',
    ]);

    return ['authorization' => $authorization, 'dashboard_id' => $dashboard_id];
```
