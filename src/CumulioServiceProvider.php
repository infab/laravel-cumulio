<?php

namespace Infab\LaravelCumulio;

use Illuminate\Support\ServiceProvider;
use Infab\LaravelCumulio\Lib\Cumulio;

class CumulioServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/services.php', 'services');
        $this->app->bind(\Infab\LaravelCumulio\Lib\Cumulio::class, function ($app) {
            return new \Infab\LaravelCumulio\Lib\Cumulio();
        });
    }
}
